﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAnimation : MonoBehaviour 
{
	public float deadZone = 5f; //is a value range thath sould be ignored, used when the ene,y wants to turn
						   		//Else the enemy would be oversteering
	private GameObject player;
	private EnemySight enemySight;
	private NavMeshAgent nav;
	private Animator anim;
	private HashIDs hash;
	private AnimatorSetup animSetup;

	private void Awake() {
		player = GameObject.FindGameObjectWithTag(Tags.player);
		enemySight = GetComponent<EnemySight>();
		nav = GetComponent<NavMeshAgent>();
		anim = GetComponent<Animator>();
		hash = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashIDs>();

		nav.updateRotation = false;
		animSetup = new AnimatorSetup(anim,hash);

		//Shooting layer is represented by a 1
		//Represents gun with 2
		anim.SetLayerWeight(1,1f);
		anim.SetLayerWeight(2,1f);

		//The animator controller measures the parameters in radians
		deadZone *= Mathf.Deg2Rad;
	}

	private void Update() {
		NavAnimSetup();
	}

	private void OnAnimatorMove() {
		//change in position per frame
		nav.velocity = anim.deltaPosition / Time.deltaTime;
		transform.rotation = anim.rootRotation;
	}

	void NavAnimSetup()
	{
		float speed;
		float angle;

		if(enemySight.playerInSight)
		{
			speed = 0f;

			//Angle between direction the enmy should face and his current facing direction
			angle = FindAngle(transform.forward, player.transform.position - transform.position, transform.up);
		}
		else//Player not in sight
		{
			speed = Vector3.Project(nav.desiredVelocity, transform.forward).magnitude;
			angle = FindAngle(transform.forward, nav.desiredVelocity, transform.up);
			if(Mathf.Abs(angle) < deadZone)
			{
				transform.LookAt(transform.position + nav.desiredVelocity);
				angle = 0f;
			}
		}

		animSetup.Setup(speed,angle);
	}

	float FindAngle(Vector3 fromVector, Vector3 toVector, Vector3 upVector)
	{
		if(toVector == Vector3.zero)
			return 0f;

		float angle = Vector3.Angle(fromVector, toVector);
		Vector3 normal = Vector3.Cross(fromVector, toVector);
		//If the normal and the up vector are pointing on the same direction then the result 
		//will be greater than zero, therefore we can multiply the angle		
		angle *= Mathf.Sign(Vector3.Dot(normal,upVector));//This will tell us the direction of our desired angular speed
		angle *= Mathf.Deg2Rad;

		return angle;
	}
}
