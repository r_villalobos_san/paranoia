﻿using System.Collections;
using UnityEngine;

public class EnemyShooting : MonoBehaviour 
{
	public float maximumDamage = 120;
	public float minimumDamage = 45f;
	public AudioClip shotClip;
	public float flashIntensity = 3f;
	public float fadeSpeed = 10f;

	private Animator anim;
	private HashIDs hash;
	private LineRenderer laserShotLine;
	private Light laserShotLight;
	private SphereCollider col;
	private Transform player;
	private PlayerHealth playerHealth;
	private bool shooting;
	private float scaledDamage;

	private void Awake() {
		// Setting up the references.
		anim = GetComponent<Animator>();
		laserShotLine = GetComponentInChildren<LineRenderer>();
		laserShotLight = laserShotLine.gameObject.GetComponent<Light>();
		col = GetComponent<SphereCollider>();
		player = GameObject.FindGameObjectWithTag(DoneTags.player).transform;
		playerHealth = player.gameObject.GetComponent<PlayerHealth>();
		hash = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashIDs>();
		
		// The line renderer and light are off to start.
		laserShotLine.enabled = false;
		laserShotLight.intensity = 0f;
		
		// The scaledDamage is the difference between the maximum and the minimum damage.
		scaledDamage = maximumDamage - minimumDamage;
	}

	private void Update() {
		float shot = anim.GetFloat(hash.shotFloat);

		if(shot > 0.5f && !shooting){
			Shoot();
		}	
		if(shot < 0.5f)
		{
			shooting = false;
			laserShotLine.enabled = false;
		}

		laserShotLight.intensity = Mathf.Lerp(laserShotLight.intensity, 0f, fadeSpeed*Time.deltaTime);
	}

	/// <summary>
	/// Callback for setting up animation IK (inverse kinematics).
	/// </summary>
	/// <param name="layerIndex">Index of the layer on which the IK solver is called.</param>
	void OnAnimatorIK(int layerIndex)
	{
		float aimWeight = anim.GetFloat(hash.aimWeightFloat);
		anim.SetIKPosition(AvatarIKGoal.RightHand, player.position+Vector3.up * 1.5f);
		anim.SetIKPositionWeight(AvatarIKGoal.RightHand, aimWeight);
	}

	void Shoot(){
		shooting = true;
		float fractionalDistance = (col.radius - Vector3.Distance(transform.position, player.position))/col.radius;
		float damage = scaledDamage*fractionalDistance + minimumDamage;
		playerHealth.TakeDamage(damage);
		ShotEffects();
	}

	void ShotEffects()
	{
		laserShotLine.SetPosition(0, laserShotLine.transform.position);
		laserShotLine.SetPosition(1,player.transform.position + Vector3.up * 1.5f);
		laserShotLine.enabled = true;
		laserShotLight.intensity = flashIntensity;
		AudioSource.PlayClipAtPoint(shotClip,laserShotLight.transform.position);
	}

}
