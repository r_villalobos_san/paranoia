﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class EnemySight : MonoBehaviour 
{
	public float fieldOfView = 110f; //110 degrees
	public bool playerInSight;
	//Personal Last sighting of the player
	public Vector3 personalLastSighting;

	//We need the length of the path between enemy and player
	private  NavMeshAgent nav;
	private SphereCollider col;
	private Animator anim;
	private Last_Player_Sighting last_Player_Sighting;
	private GameObject player;
	private Animator playerAnim;
	private PlayerHealth playerHealth;
	private HashIDs hash;
	private Vector3 previousSighting;//Store last player position to know if his position has changed

	private void Awake() {
		nav = GetComponent<NavMeshAgent>();
		col = GetComponent<SphereCollider>();
		anim = GetComponent<Animator>();
		last_Player_Sighting = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<Last_Player_Sighting>();
		player = GameObject.FindGameObjectWithTag(Tags.player);
		playerAnim = player.GetComponent<Animator>();
		playerHealth = player.GetComponent<PlayerHealth>();
		hash = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashIDs>();

		//personal last sighting and previous sighting must be set to default reset position
		//This way enemies will not chase the player when the game starts
		personalLastSighting = last_Player_Sighting.resetPosition;
		previousSighting = last_Player_Sighting.resetPosition;
		//Debug.Log(playerHealth.health);
	}

	private void Update() 
	{
		
		//Global sighting of the player has changed?
		if(last_Player_Sighting.position != previousSighting)
		{
			//If it has changed change the personal sighting of the enemy
			personalLastSighting = last_Player_Sighting.position;
		}
		//change the previous position to be this frame's position
		previousSighting = last_Player_Sighting.position;

		//Set player in sight animator paramter only if th eplayer is alive
		if(playerHealth.health > 0f)
		{
			anim.SetBool(hash.playerInSightBool, playerInSight);
		}
		else
		{
			anim.SetBool(hash.playerInSightBool,false);
		}
	}


	private void OnTriggerStay(Collider other) {

		if(other.gameObject == player)
		{
			playerInSight = false;
			Vector3 direction = other.transform.position - transform.position;
			float angle = Vector3.Angle(direction, transform.forward);

			//if the angle between forward vector of enemy and player is less than hlaf the 
			//field of view of the enemy then...
			if(angle < (fieldOfView*0.5f))
			{
				//Is there an obstruction? Let's use a raycast!!!
				//The direction vector of a raycast is always normalized
				RaycastHit hit;
				if(Physics.Raycast(transform.position+transform.up, direction.normalized,out hit, col.radius))
				{
					//did we hit something
					if(hit.collider.gameObject == player)//Did we hited the player?
					{
						playerInSight = true;
						last_Player_Sighting.position = player.transform.position;
					}
				}
			}

			//Now we need to detect if the player has been heard rather than seen
			//If the player is running or shouting
			//These are 2 states from our player Animator controller so we can use this at our
			//advantage
			int playerLayerZeroStateHash = playerAnim.GetCurrentAnimatorStateInfo(0).nameHash;
			int playerLayerOneStateHash = playerAnim.GetCurrentAnimatorStateInfo(1).nameHash;

			if(playerLayerZeroStateHash == hash.locomotionState || playerLayerOneStateHash == hash.shoutState)
			{
				Debug.Log("Shouting heard");
				//If the distance from sound is less or equal than the range of enemy
				if(CalculatePathLength(player.transform.position) <= col.radius)
				{
					personalLastSighting = player.transform.position;
				}
			}
		}	
	}


	void OnTriggerExit(Collider other)
	{
		if(other.gameObject == player)
		{
			playerInSight = false;
		}
	}

	//We need t calculate the distance the sound has to traverse to see if the enemy can hear the player
	//He shouldn;t be able to hear if we are on a different room
	float CalculatePathLength(Vector3 targetPosition)
	{
		NavMeshPath path = new NavMeshPath();
		//Calculate path
		if(nav.enabled)
		{
			nav.CalculatePath(targetPosition, path);
		}

		//path now contains the data that tells us how many corners have been found on the navmesh path
		//This array will help us to calculate the full distance between all waypoints
		//We are gonna sum all the distances.
		Vector3[] allWayPoints = new Vector3[path.corners.Length+2]; //+2 BECAUSE we added the player and the enemy as points
		allWayPoints[0] = transform.position;
		allWayPoints[allWayPoints.Length-1] = targetPosition;

		for (int i = 0; i < path.corners.Length; i++)
		{
			//We dont assign the first item
			allWayPoints[i+1] = path.corners[i];
		}
		float pathLength = 0f;

		//Iterate over each of the lengths of the waypoints
		for (int i = 0; i < allWayPoints.Length-1; i++)
		{
			pathLength += Vector3.Distance(allWayPoints[i], allWayPoints[i+1]);
		}
		return pathLength;
	}
}
