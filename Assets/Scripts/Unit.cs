﻿using UnityEngine;

public abstract class Unit : MonoBehaviour 
{

	public int TeamNumber;

	protected Animator _anim;

	protected void Awake()
	{
		_anim = GetComponentInChildren<Animator>();
		
		Color teamColor = GameManager.Instance.TeamColors[TeamNumber];
		transform.Find("Teddy/Teddy_Body").GetComponent<Renderer>().material.color = teamColor;

		UnitAwake();
	}

	protected abstract void UnitAwake();

}
