﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

public AudioClip shoutingClip;//Sound for distracting
public float turnSmoothing = 15f;//Smoothing value for lerping movement of player
public float speedDampTime = 0.1f;
private Animator anim;
private HashIDs hash;
private float _X_input;
private float _Y_input;
private bool shout;
private bool sneak;

private void Awake() {
	anim = GetComponent<Animator>();
	hash = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashIDs>();
	//set weight of shouting layer
	//the weight of the layer is it;s effectiveness
	anim.SetLayerWeight(1, 1f); 
}

	void FixedUpdate()
	{

		MovemenetManagement(_X_input,_Y_input,sneak, shout);
	}

	private void Update() {
		_X_input = Input.GetAxis("Horizontal");
		_Y_input = Input.GetAxis("Vertical");
		sneak = Input.GetButton("Sneak");
		shout = Input.GetButtonDown("Attract");
		AudioManagement(shout);
	}

	void MovemenetManagement(float horizontal,float vertical, bool sneaking, bool shouting)
	{	
		
		anim.SetBool(hash.shoutingBool, shout);
		anim.SetBool(hash.sneakingBool, sneaking);

		if(horizontal != 0 || vertical != 0f)
		{
			Rotating(horizontal,vertical);
			anim.SetFloat(hash.speedFloat, 5.5f,speedDampTime,Time.deltaTime);
		}
		else//If we are not receiving any input
		{
			anim.SetFloat(hash.speedFloat, 0f);
		}
	}

	void Rotating(float horizontal, float vertical)
	{
		Vector3 targetDirection = new Vector3(horizontal, 0f, vertical);
		Quaternion targetRotation = Quaternion.LookRotation(targetDirection,Vector3.up);
		Quaternion newRotation = Quaternion.Lerp(GetComponent<Rigidbody>().rotation, targetRotation,turnSmoothing*Time.deltaTime);
		GetComponent<Rigidbody>().MoveRotation(newRotation);
	}

	void AudioManagement(bool shout)
	{
		AudioSource audio = GetComponent<AudioSource>();
		//only play while in the locomotion state
		//If we are currently on the locomotion state
		if(anim.GetCurrentAnimatorStateInfo(0).nameHash == hash.locomotionState)
		{
			//if clip is not already playing
			if(!audio.isPlaying)
			{
				audio.Play();
			}
			else
			{
				audio.Stop();
			}

			if(shout)
			{
				AudioSource.PlayClipAtPoint(shoutingClip, transform.position);
			}
		}
	}

}
