﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class LaserSwitchDeactivation : MonoBehaviour 
{	
	public GameObject laser;
	public Material unlockMat;
	private GameObject player;
	private AudioSource audio;

	private void Awake() {
		player = GameObject.FindGameObjectWithTag(Tags.player);
		audio = GetComponent<AudioSource>();
	}

	 /// <summary>
	/// OnTriggerStay is called once per frame for every Collider other
	/// that is touching the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerStay(Collider other)
	{
		if (other.gameObject == player)
		{
			if (Input.GetButton("Switch"))
			{
				LaserDeactivation();
			}
		}
	}

	void LaserDeactivation(){
		laser.SetActive(false);

		Renderer screen = transform.Find("prop_switchUnit_screen").GetComponent<Renderer>();
		screen.material = unlockMat;
		audio.Play();
	}
}
