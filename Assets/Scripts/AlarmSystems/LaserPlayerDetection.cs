﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class LaserPlayerDetection : MonoBehaviour 
{
	private GameObject player;
	private Last_Player_Sighting _lastPlayerSighting;
	private Renderer renderer;

	void Awake(){
		player = GameObject.FindGameObjectWithTag(Tags.player);
		_lastPlayerSighting = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<Last_Player_Sighting>();
		renderer = GetComponent<Renderer>();
	}

	/// <summary>
	/// OnTriggerStay is called once per frame for every Collider other
	/// that is touching the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerStay(Collider other)
	{
		if(renderer.enabled)
		{
			if(other.gameObject == player)
			{
				_lastPlayerSighting.position = other.transform.position;
			}
		}
	}

}
