﻿using System.Collections;
using UnityEngine;

public class CCTVPlayerDetection : MonoBehaviour 
{

	private GameObject player;
	private Last_Player_Sighting lastPlaterSighting;

	private void Awake() {
		player = GameObject.FindGameObjectWithTag(Tags.player);
		lastPlaterSighting = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<Last_Player_Sighting>();
	}

	/// <summary>
	/// OnTriggerStay is called once per frame for every Collider other
	/// that is touching the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerStay(Collider other)
	{
		if (other.gameObject == player)
		{
			Vector3 relPlayerPos = player.transform.position - transform.position;
			RaycastHit hit;

			if(Physics.Raycast(transform.position,relPlayerPos,out hit))
			{
				if(hit.collider.gameObject == player)
				{
					lastPlaterSighting.position = player.transform.position;
				}
			}
		}
	}
}
