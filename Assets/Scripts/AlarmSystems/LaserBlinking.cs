﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
[RequireComponent(typeof(Light))]
public class LaserBlinking : MonoBehaviour {

	public float onTime;//Amount of time lasers are on and off
	public float offTime;
	private float timer;//Timer to switch between on and off
	private Renderer renderer;
	private Light light;

	private void Awake() {
		renderer = GetComponent<Renderer>();
		light = GetComponent<Light>();
	}

	private void Update() {
		timer =+ Time.deltaTime;

		if (renderer.enabled && timer >= onTime)
		{
			SwitchBeam();
		}

		if (!renderer.enabled && timer >= offTime)
		{
			SwitchBeam(); 
		}
	}

	void SwitchBeam(){
		timer = 0f;
		renderer.enabled = !renderer.enabled;
		light.enabled = !light.enabled;
	}
}
