﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(GUITexture))]
public class SceneFadeInOut : MonoBehaviour {

	public float fadeSpeed = 1.5f;
	private bool sceneStarting = true;
	private GUITexture _GUI_texture;

	private void Awake() {
		//Rect represents rectangle
		_GUI_texture = GetComponent<GUITexture>();
		_GUI_texture.pixelInset = new Rect(0f,0f,Screen.width, Screen.height);
	}

	private void Update() {
		if(sceneStarting){
			StartScene();
		}
	}

	void FadeToClear(){
		_GUI_texture.color = Color.Lerp(_GUI_texture.color, Color.clear, fadeSpeed*Time.deltaTime);
	}

	void FadeToBlack(){
		_GUI_texture.color = Color.Lerp(_GUI_texture.color, Color.black, fadeSpeed*Time.deltaTime);
	}

	void StartScene(){
		FadeToClear();
		//Lerp is changen by a percentage of the differnece between two values meaning that it will take a long time to reach the target color.
		//The color is close to clear whrn it's alpha is low
		if(_GUI_texture.color.a <= 0.05f){
			_GUI_texture.color = Color.clear;
			_GUI_texture.enabled = false;
			sceneStarting = false;
		}
	}

	public void EndScene(){
		_GUI_texture.enabled = true;
		FadeToBlack();

		if(_GUI_texture.color.a >= 0.95f){
			Application.LoadLevel(1);
		}
	}
}
