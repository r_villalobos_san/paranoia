﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class AIController : Unit 
{
	#region Global Variables

		private NavMeshAgent _Agent;
		private IEnumerator _CurrentState;
		private Outpost _TargetOutpost = null;

    #endregion Global Variables

    #region Lifecycle

	    protected override void UnitAwake()
		{
			_Agent = GetComponent<NavMeshAgent>();
			StopAllCoroutines();
			SetState(State_Idle());//sET MY DEFAULT STATE
		}



		private void Update()
		{
			_anim.SetFloat("VerticalMovement",_Agent.velocity.magnitude);
		}

	#endregion Lifecycle

	#region StateMachine

	private void SetState(IEnumerator newState)
	{
		if(_CurrentState != null)
		{
			StopCoroutine(_CurrentState);
		}

		_CurrentState = newState;
		StartCoroutine(_CurrentState);
	}

	IEnumerator State_Idle()
	{
		while (_TargetOutpost == null)
		{
			LookForOutpost();
			yield return null;
		}

		SetState(State_MovingToOutpost());
	}



    IEnumerator State_MovingToOutpost()
    {
		_Agent.SetDestination(_TargetOutpost.transform.position);
		while(_Agent.remainingDistance > _Agent.stoppingDistance)
		{
			yield return null;
		}

		SetState(State_CapturingOutpost());
    }

    IEnumerator State_CapturingOutpost()
    {

		while(_TargetOutpost.CurrentTeam != TeamNumber || _TargetOutpost.CaptureValue < 1f)
		{
			yield return null;
		}

		_TargetOutpost = null;
        SetState(State_Idle());
    }

    private void LookForOutpost()
    {
		int r = Random.Range(0, Outpost.OutpostList.Count);
		Outpost temp = Outpost.OutpostList[r];
		//If not fully captured of not fully captured by the other team then...
		if(temp.CaptureValue < 1f || temp.CurrentTeam != TeamNumber)
		{
			_TargetOutpost = temp;
		}
		else
		{
			_TargetOutpost = null;
		}
    }

    #endregion StateMachine
}
