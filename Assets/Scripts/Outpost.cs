﻿using System.Collections.Generic;
using UnityEngine;

public class Outpost : MonoBehaviour 
{
	internal static List<Outpost> OutpostList = new List<Outpost>();
	[SerializeField]
	private float _CaptureTime = 5f;
	internal float CaptureValue = 0; //Internal is a public variable hidden in ispector
									 //Only apears in the same namespace-> our ap
	internal int CurrentTeam = 0;
	public SkinnedMeshRenderer _FlagRenderer;

	private void Awake()
	{
		_FlagRenderer = GetComponentInChildren<SkinnedMeshRenderer>();
		OutpostList.Add(this);
	}

	private void Update()
	{
		Color flagColor = GameManager.Instance.TeamColors[CurrentTeam];
		_FlagRenderer.material.color = Color.Lerp(Color.white, flagColor, CaptureValue);
	}

	//OnTrigger stay belongs to physics
	//Thats why we use fixedDeltaTime, because it runs on fixed update logic
	private void OnTriggerStay(Collider other)
	{
		Unit unit = other.GetComponent<Unit>();
		if(unit == null){return;}

		if(unit.TeamNumber == CurrentTeam)
		{
			CaptureValue += Time.fixedDeltaTime / _CaptureTime;
			if(CaptureValue > 1f)
			{
				CaptureValue = 1;
			}
		}
		else
		{
			CaptureValue -= Time.fixedDeltaTime / _CaptureTime;
			if(CaptureValue <= 0)
			{
				CaptureValue = 0;
				CurrentTeam = unit.TeamNumber;
			}
		}
	}

}
