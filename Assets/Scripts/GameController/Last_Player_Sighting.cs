﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Last_Player_Sighting : MonoBehaviour {

	public Vector3 position = new Vector3(1000f,1000f,1000f);
	public Vector3 resetPosition = new Vector3(1000f,1000f,1000f);
	public float lightHighIntensity = 0.25f;
	public float lightLowIntensity = 0f;
	public float fadeSpeed = 7f;
	public float musicFadeSpeed = 1f;
	private AlarmLight alarm;
	private Light mainLight;
	private AudioSource panicAudio;
	private AudioSource[] sirens;
	private void Awake() 
	{
		alarm = GameObject.FindGameObjectWithTag(Tags.alarm).GetComponent<AlarmLight>();
		mainLight = GameObject.FindGameObjectWithTag(Tags.mainLight).GetComponent<Light>(); 
		panicAudio = GetComponentInChildren<AudioSource>();
		//GameObject[] sirenGameObjects  = GameObject.FindGameObjectsWithTag(Tags.siren);
		// sirens = new AudioSource[sirenGameObjects.Length];
		// for (int i = 0; i < sirens.Length; i++)
		// {
		// 	sirens[i] =  sirenGameObjects[i].GetComponent<AudioSource>();
		// }
	}

	private void Update() {
		SwitchAlarms();
		MusicFading();
	}

	void SwitchAlarms(){
		alarm.alarmOn = (position != resetPosition);
		float newIntensity;
		if(position != resetPosition){
			newIntensity = lightLowIntensity;
		}
		else
		{
			newIntensity = lightHighIntensity;
		}

		mainLight.intensity = Mathf.Lerp(mainLight.intensity, newIntensity,fadeSpeed*Time.deltaTime);

		// //Tell the sirens whether or not to play
		// for (int i = 0; i < sirens.Length; i++)
		// {
		// 	//if the position is not the default position and sirens are not playing something then they should be playing
		// 	if(position != resetPosition && !sirens[i].isPlaying
		// 	){
		// 		sirens[i].Play();
		// 	}
		// 	else if(position == resetPosition)// if the position is the reset position the stop playing
		// 	{
		// 		sirens[i].Stop();
		// 	}
		// }
	}

	void MusicFading()
	{
		AudioSource audio = gameObject.GetComponent<AudioSource>();
		if (position != resetPosition)//If alarm should be going off
		{
			audio.volume = Mathf.Lerp(audio.volume, 0f, musicFadeSpeed*Time.deltaTime);
			panicAudio.volume = Mathf.Lerp(panicAudio.volume, 0.8f, musicFadeSpeed*Time.deltaTime);
		}
		else //If position is the reset position then play normal music again
		{
			audio.volume = Mathf.Lerp(audio.volume, 0.8f, musicFadeSpeed*Time.deltaTime);
			panicAudio.volume = Mathf.Lerp(panicAudio.volume, 0f, musicFadeSpeed*Time.deltaTime);
		}
	}

}
