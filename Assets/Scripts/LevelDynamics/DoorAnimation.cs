﻿using System.Collections;
using UnityEngine;
[RequireComponent(typeof(Animator))]
public class DoorAnimation : MonoBehaviour 
{
	public bool requireKey;
	public AudioClip doorSwishClip;
	public AudioClip accessDeniedClip;

	private Animator anim;
	private HashIDs hash;
	private GameObject player;
	private PlayerInventory playerInventory;

	//We want enemies to be able to open the door.
	//Door should be open if there are more than 1 collider on his triggering area.
	private int count;
	void Awake(){
		anim = GetComponent<Animator>();
		hash = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<HashIDs>();
		player = GameObject.FindGameObjectWithTag(Tags.player);
		playerInventory = player.GetComponent<PlayerInventory>();
	}

	private void OnTriggerEnter(Collider other) {
		if(other.gameObject == player){
			if(requireKey)
			{
				if(playerInventory.hasKey)
				{
					count++;
				}
				else{
					GetComponent<AudioSource>().clip = accessDeniedClip;
					GetComponent<AudioSource>().Play();
				}
			}
			else
			{
				count++;
			}
		}
		else if(other.gameObject.tag == Tags.enemy)
		{
			if(other is CapsuleCollider)
			{
				count++;
			}
		}
	}

	/// <summary>
	/// OnTriggerExit is called when the Collider other has stopped touching the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerExit(Collider other)
	{
		if(other.gameObject == player || (other.tag == Tags.enemy && other is CapsuleCollider))
		{
			count = Mathf.Max(0,count-1);
		}
	}

     /// <summary>
	/// Update is called every frame, if the MonoBehaviour is enabled.
	/// </summary>
	void Update()
	{
		//Debug.Log(anim);
		anim.SetBool(hash.openBool, count > 0);
		if(anim.IsInTransition(0) && !GetComponent<AudioSource>().isPlaying)
		{
			GetComponent<AudioSource>().clip = doorSwishClip;
			GetComponent<AudioSource>().Play();
		}
	}

}
