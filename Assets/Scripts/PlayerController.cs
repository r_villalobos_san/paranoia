﻿using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerController : Unit 
{
	
	#region Global Variables

		[SerializeField]
		private float _MoveSpeed = 5f;

		[SerializeField]
		private float _Sprint_Speed_Mult = 5f;

		[SerializeField]
		private float _JumpSpeed = 12f;

		private Rigidbody _RB;
		private Transform _Camera;

		private float _X_Input;
		private float _Z_Input;
		private float _Speed_Mult = 1f;
    	private bool _Jump_Pressed;

		[SerializeField]
		private Transform _Camera_Pivot;


    #endregion Global Variables

    #region Life Cycle

		protected override void UnitAwake()
		{
			_RB = GetComponent<Rigidbody>();
			_Camera = Camera.main.transform;
		}


		private void Update()
		{
			Cursor.lockState = CursorLockMode.Locked;

			ReadMoveInputs();
			CameraRotations();
			CameraZoom();
			SetAnimationValues();
		}

		private void FixedUpdate()
		{
			ApplyMovementPhysics();

		}


    #endregion Unity Functions

    #region Class Functions

    	private void ReadMoveInputs()
		{
			//Movement Inputs
			_X_Input = Input.GetAxis("Horizontal");
			_Z_Input = Input.GetAxis("Vertical");
			_Speed_Mult = Input.GetKey(KeyCode.LeftShift) ? _Sprint_Speed_Mult : 1;
			//_Jump_Pressed |= Input.GetKeyDown(KeyCode.Space); //NOTE THE OR EQUALS OPERSATOR!!!=>		|=

			if(Input.GetKeyDown(KeyCode.Space))
			{
				_Jump_Pressed = true;
				_anim.SetTrigger("JumpTrigger");
			}
    	}

		private void CameraRotations()
    	{
			//Rotations
			float mouseX = Input.GetAxis("Mouse X");
			float mouseY = Input.GetAxis("Mouse Y");

			transform.Rotate(0, mouseX, 0);
			_Camera_Pivot.Rotate(-mouseY, 0, 0);
			
			//Debug.Log(_Camera_Pivot.rotation.eulerAngles);
    	}

		private void CameraZoom()
    	{
			//Camera Zoom
			Vector3 newZoom = _Camera.localPosition;
			newZoom.z += Input.mouseScrollDelta.y;// += the value fo the scroll Wheel
			newZoom.z = Mathf.Clamp(newZoom.z, -24f, 0f);
			_Camera.localPosition = newZoom;
    	}

		private void ApplyMovementPhysics()
    	{
			Vector3 newVel = new Vector3(_X_Input, 0, _Z_Input) * _MoveSpeed * _Speed_Mult;
			newVel = transform.TransformVector(newVel); //Transform the vector from global coordinates to local coordinates
														//We assign the y velocity separately because we are multiplying times _MoveSpeed.
			newVel.y = _Jump_Pressed ? _JumpSpeed : _RB.velocity.y;
			_RB.velocity = newVel;
			_Jump_Pressed = false;
    	}

		private void SetAnimationValues()
		{
			_anim.SetFloat("SpeedMult", _Speed_Mult);//Applies a multiplier for the speed of the animation on the animator controller
			_anim.SetFloat("HorizontalMovement", _X_Input);
			_anim.SetFloat("VerticalMovement", _Z_Input);
		}

	#endregion Life Cycle

}
