﻿Shader "Rodrigo/Spotlight"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_CharactersPosition("Char pos", vector) = (0,0,0,0)
		_CircleRadius("Spotlight size", Range(0,20)) = 3
		_RingSize("Ring size", Range(0,5)) = 1
		_ColorTin("Outside of the spotlight color", color) = (0,0,0,0)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float dist : TEXCOORD1;
				
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;

			float4 _CharactersPosition;
			float _CircleRadius;
			float _RingSize;
			float4 _ColorTin;

			
			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				float3 worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
				o.dist = distance(worldPos, _CharactersPosition.xyz); //Put the distance right away


				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				// sample the texture
				fixed4 col = _ColorTin; // (0,0,0,0)

				//Distance between pixel and character

				//Spotlight section
				if(i.dist < _CircleRadius)
					col = tex2D(_MainTex, i.uv);

				//Ring section -> Blending in between
				else if(i.dist > _CircleRadius && i.dist < _CircleRadius + _RingSize)
				{
					float blendStrength = i.dist - _CircleRadius; //How far we made it inside of the ring?
					col = lerp(tex2D(_MainTex, i.uv), _ColorTin, blendStrength / _RingSize);
				}

				//Outer Section

				return col;
			}
			ENDCG
		}
	}
}
